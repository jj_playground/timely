class TeachersController < ApplicationController
  before_action :set_teacher, only: [:edit, :update]
  def index
    @teachers = Teacher.all
  end

  def edit
  end

  def new
    @teacher = Teacher.new
  end

  def create
    @teacher = Teacher.new(teacher_params)
    respond_to do |format|
      if @teacher.save
        format.html { redirect_to teachers_url, notice: "Teacher was successfully saved." }
        format.json { render :show, status: :ok, location: @teacher }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @teacher.update(teacher_params)
        format.html { redirect_to teachers_url, notice: "Teacher was successfully saved." }
        format.json { render :show, status: :ok, location: @teacher }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  def teacher_params
    params.require(:teacher).permit(:name, :email, :tel)
  end

  def set_teacher
    @teacher = Teacher.find(params[:id])
  end
end
