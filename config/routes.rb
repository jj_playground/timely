Rails.application.routes.draw do
  resources :teachers
  root to: 'welcome#index'
end
